import { defaultRequest, jsonResponse }                 from '../types'
import { createServiceCenterFromModel, IServiceCenter } from '../helpers/service-centers'
import ServiceCenter                                    from '../models/service-center/service-center.model'
import ServiceCenterDeviceService
                                                        from '../models/service-center/service-center-device-service.model'

export async function getServiceCentersList(req: defaultRequest): jsonResponse<{ serviceCenters: IServiceCenter[] }> {
  return {
    status: 1,
    serviceCenters: await Promise.all((await ServiceCenter.findAll()).map(serviceCenter => createServiceCenterFromModel({ serviceCenter }))),
  }
}

export async function createServiceCenter({ body }: defaultRequest): jsonResponse {
  const serviceCenter: IServiceCenter = body.serviceCenter
  const serviceCenterModel = await ServiceCenter.create({
    name: serviceCenter.name,
    address: serviceCenter.address,
    phone: serviceCenter.phone,
  })

  await Promise.all(serviceCenter.devicesServices.map(async (deviceService) => {
    await ServiceCenterDeviceService.create({
      serviceCenterId: serviceCenterModel.id,
      deviceServiceId: deviceService.id,
    })
  }))

  return {
    status: 1,
  }
}

export async function editServiceCenter({ body, params }: defaultRequest): jsonResponse {
  const serviceCenterModel = await ServiceCenter.findByPk(params.id, { include: [ServiceCenterDeviceService] })
  if (!serviceCenterModel) return {
    status: 0,
    code: 404,
    error: 'Не найдено',
  }

  const serviceCenter: IServiceCenter = body.serviceCenter
  serviceCenterModel.name = serviceCenter.name
  serviceCenterModel.phone = serviceCenter.phone
  serviceCenterModel.address = serviceCenter.address
  await serviceCenterModel.save()

  await Promise.all(serviceCenter.devicesServices.map(async (deviceService) => {
    if (!serviceCenterModel.devicesServices.some(x => x.deviceServiceId === deviceService.id)) await ServiceCenterDeviceService.create({
      serviceCenterId: serviceCenterModel.id,
      deviceServiceId: deviceService.id,
    })
  }))

  await Promise.all(
    serviceCenterModel.devicesServices
                      .filter(x => !serviceCenter.devicesServices.some(y => y.id === x.deviceServiceId))
                      .map(x => x.destroy()),
  )

  return {
    status: 1,
  }
}

export async function deleteServiceCenter({ params }: defaultRequest): jsonResponse {
  const serviceCenterModel = await ServiceCenter.findByPk(params.id, { include: [ServiceCenterDeviceService] })
  if (!serviceCenterModel) return {
    status: 0,
    code: 404,
    error: 'Не найдено',
  }

  await serviceCenterModel.destroy()

  return {
    status: 1,
  }
}
