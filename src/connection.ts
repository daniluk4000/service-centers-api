import { Sequelize } from 'sequelize-typescript'
import { config }    from 'dotenv-flow'

config()

const sequelize = new Sequelize({
  database: process.env.DATABASE_NAME,
  username: process.env.DATABASE_USER,
  host: process.env.DATABASE_URL,
  password: process.env.DATABASE_PWD,
  dialect: 'mysql',
  port: 3306,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },

  logging: false,
  models: [__dirname + '/models/**/*.model.ts', __dirname + '/models/**/*.model.js'],
})

export default sequelize
