import { defaultRequest, jsonResponse, userAuthRequest }                     from '../types'
import { authUserByEmailAndPassword, createUserFromModel, IUser, IUserFull } from '../helpers/user'
import { randomBytes }                                                       from 'crypto'
import UserToken                                                             from '../models/user/user-token.model'
import { hash }                                                              from 'bcrypt'
import User                                                                  from '../models/user/user.model'
import ServiceCenterApplication
                                                                             from '../models/service-center/service-center-application.model'

export async function usersGet(): jsonResponse<{ users: IUserFull[] }> {
  return {
    status: 1,
    users: await Promise.all((await User.findAll({ include: [ServiceCenterApplication] })).map(user => createUserFromModel({
      user,
      full: true,
    }))),
  }
}

export async function userLogin({ body: { email, password } }: defaultRequest): jsonResponse<{ token: string, user: IUserFull }> {
  const user = await authUserByEmailAndPassword(email, password)
  if (typeof user === 'string') return {
    status: 0,
    code: 401,
    error: user,
  }

  const token = randomBytes(16).toString('hex')
  await UserToken.create({
    token,
    userId: user.id,
  })

  return {
    status: 1,
    token,
    user: await createUserFromModel({ user, full: true }),
  }
}

export async function userGet({ user }: userAuthRequest): jsonResponse<{ user: IUserFull }> {
  return {
    status: 1,
    user: await createUserFromModel({ user, full: true }),
  }
}

export async function userRegister({ body }: userAuthRequest): jsonResponse<{ token: string, user: IUserFull }> {
  const user: Partial<IUser & { password: string }> = body.user

  if (!user.email) return {
    status: 0,
    code: 404,
    error: 'Не передано поле email',
  }

  if (!user.password) return {
    status: 0,
    code: 404,
    error: 'Не передано поле пароля',
  }

  if (!user.firstName) return {
    status: 0,
    code: 404,
    error: 'Не передано поле имени',
  }

  if (!user.lastName) return {
    status: 0,
    code: 404,
    error: 'Не передано поле фамилии',
  }

  const password = await hash(user.password, 8)

  const userModel = await User.create({
    email: user.email,
    password,
    firstName: user.firstName,
    lastName: user.lastName,
    middleName: user.middleName,
    address: user.address,
    phone: user.phone,
  })

  const token = randomBytes(16).toString('hex')
  await UserToken.create({
    token,
    userId: userModel.id,
  })

  return {
    status: 1,
    token,
    user: await createUserFromModel({ user: userModel, full: true }),
  }
}

export async function userEdit({ user: userModel, body, params: { id } }: userAuthRequest): jsonResponse {
  const user: IUser = body.user

  if (id && userModel.role === 'admin') {
    const foundUser = await User.findByPk(id)
    if (!foundUser) return {
      status: 0,
      code: 404,
      error: 'Не найдено',
    }

    userModel = foundUser
  }

  userModel.email = user.email
  userModel.firstName = user.firstName
  userModel.lastName = user.lastName
  userModel.middleName = user.middleName
  userModel.phone = user.phone
  userModel.address = user.address
  await userModel.save()

  return {
    status: 1,
  }
}

export async function userDelete({ params: { id } }: defaultRequest): jsonResponse {
  const foundUser = await User.findByPk(id)
  if (!foundUser) return {
    status: 0,
    code: 404,
    error: 'Не найдено',
  }

  if(foundUser.role === 'admin') return {
    status: 0,
    code: 403,
    error: "Вы не можете удалить этого пользователя"
  }

  await foundUser.destroy()

  return {
    status: 1,
  }
}
