import { createDeviceServiceFromModel, IDeviceService } from './devices-services'
import ServiceCenter                                    from '../models/service-center/service-center.model'
import DeviceService                                    from '../models/service/device-service.model'
import { ValidationError }                              from 'sequelize'
import { createUserFromModel, IUser }                   from './user'
import ServiceCenterApplication                         from '../models/service-center/service-center-application.model'

export interface IServiceCenter {
  id: number
  name: string
  address: string
  phone: string
  devicesServices: IDeviceService[]
}

export interface IServiceCenterApplication {
  id: number
  status: string
  totalCost: number

  userId: number
  user: IUser

  serviceCenterId: number
  serviceCenter: IServiceCenter

  devicesServices: IDeviceService[]
}

export async function createServiceCenterFromModel({ serviceCenter }: { serviceCenter: ServiceCenter }): Promise<IServiceCenter> {
  const devicesServicesCenter = serviceCenter.devicesServices || await serviceCenter.$get('devicesServices', { include: [DeviceService] })
  const devicesServices: DeviceService[] = await Promise.all(devicesServicesCenter.map(async (deviceService) => {
    const ds = deviceService.deviceService || await deviceService.$get('deviceService')
    if (!ds) throw new ValidationError('No deviceService found in createServiceCenterFromModel')

    return ds
  }))

  return {
    id: serviceCenter.id,
    name: serviceCenter.name,
    address: serviceCenter.address,
    phone: serviceCenter.phone,
    devicesServices: await Promise.all(devicesServices.map(deviceService => createDeviceServiceFromModel({ deviceService }))),
  }
}

export async function createApplicationFromModel({ application }: { application: ServiceCenterApplication }): Promise<IServiceCenterApplication> {
  const devices = application.deviceServices || await application.$get('deviceServices')
  const user = application.user || await application.$get('user')
  if (!user) throw new ValidationError('User not found in createApplicationFromModel')

  const serviceCenter = application.serviceCenter || await application.$get('serviceCenter')
  if (!serviceCenter) throw new ValidationError('serviceCenter not found in createApplicationFromModel')

  return {
    id: application.id,
    status: application.status,
    totalCost: application.totalCost,

    userId: application.userId,
    user: await createUserFromModel({ user }),

    serviceCenterId: application.serviceCenterId,
    serviceCenter: await createServiceCenterFromModel({ serviceCenter }),

    devicesServices: await Promise.all(devices.map((async (deviceService) => {
      const ds: DeviceService | null | undefined = await (await deviceService.$get('serviceCenterDeviceService'))?.$get('deviceService')
      if (!ds) throw new ValidationError('Device Service not found in createApplicationFromModel')

      return await createDeviceServiceFromModel({ deviceService: ds })
    }))),
  }
}
