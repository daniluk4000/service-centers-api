import { Router }                from 'express'
import { usersRouter }           from './user'
import { servicesRouter }        from './services'
import { serviceCenterRouter }   from './service-centers'
import { devicesServicesRouter } from './devices-services'
import { devicesRouter }         from './devices'
import { applicationsRouter }    from './applications'

const router = Router()

router.use('/users', usersRouter)
router.use('/services', servicesRouter)
router.use('/service-centers', serviceCenterRouter)
router.use('/devices-services', devicesServicesRouter)
router.use('/devices', devicesRouter)
router.use('/applications', applicationsRouter)

export const indexRouter = router
