import {
  AutoIncrement,
  BelongsTo,
  Column,
  CreatedAt,
  DataType,
  ForeignKey,
  HasMany,
  Index,
  Model,
  PrimaryKey,
  Table,
}                                            from 'sequelize-typescript'
import User                                  from '../user/user.model'
import ServiceCenter                         from './service-center.model'
import ServiceCenterApplicationDeviceService from './service-center-application-device-service.model'

@Table({
  tableName: 'service-centers-applications',
})
export default class ServiceCenterApplication extends Model<ServiceCenterApplication> {
  @AutoIncrement
  @PrimaryKey
  @Column(DataType.INTEGER)
  id: number

  @Index
  @ForeignKey(() => User)
  @Column(DataType.INTEGER)
  userId: number

  @BelongsTo(() => User, { onDelete: 'cascade' })
  user: User

  @Index
  @ForeignKey(() => ServiceCenter)
  @Column(DataType.INTEGER)
  serviceCenterId: number

  @BelongsTo(() => ServiceCenter, { onDelete: 'cascade' })
  serviceCenter: ServiceCenter

  @HasMany(() => ServiceCenterApplicationDeviceService, { onDelete: 'cascade' })
  deviceServices: ServiceCenterApplicationDeviceService[]

  @CreatedAt
  date: Date

  @Column(DataType.STRING)
  status: string

  @Column(DataType.INTEGER)
  totalCost: number
}
