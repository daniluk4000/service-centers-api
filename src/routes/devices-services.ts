import { Router }        from 'express'
import { userAuthAdmin } from '../middlewares/userAuth'
import jsonResponseUser  from '../middlewares/jsonResponseUser'
import {
  createDeviceService,
  deleteDeviceService,
  editDeviceService,
  getDevicesServicesList,
}                        from '../controllers/devices-services'

const router = Router()

router.get('/', jsonResponseUser(getDevicesServicesList))
router.post('/', userAuthAdmin, jsonResponseUser(createDeviceService))
router.put('/:id', userAuthAdmin, jsonResponseUser(editDeviceService))
router.delete('/:id', userAuthAdmin, jsonResponseUser(deleteDeviceService))

export const devicesServicesRouter = router
