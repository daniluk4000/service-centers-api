import { Router }                                                                 from 'express'
import { userAuth, userAuthAdmin }                                                from '../middlewares/userAuth'
import jsonResponseUser                                                           from '../middlewares/jsonResponseUser'
import { createApplication, deleteApplication, editApplication, getApplications } from '../controllers/applications'

const router = Router()

router.get('/', userAuthAdmin, jsonResponseUser(getApplications))
router.post('/', userAuth, jsonResponseUser(createApplication))
router.put('/:id', userAuthAdmin, jsonResponseUser(editApplication))
router.delete('/:id', userAuth, jsonResponseUser(deleteApplication))

export const applicationsRouter = router
