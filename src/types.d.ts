import { Request } from 'express'
import User        from './models/user/user.model'

export interface defaultRequest extends Request {
  error?: string
}

export interface userAuthRequest extends defaultRequest {
  user: User
}

export type jsonResponse<T = {}> = Promise<(NonNullable<T> & jsonResponseSuccess) | jsonResponseError | jsonResponseDefault>

export type jsonResponseSuccess = {
  status: 1,
  code?: number
}

export type jsonResponseError = {
  status: 0,
  code: number,
  error: string
}

export type jsonResponseDefault = {
  status: -1
}
