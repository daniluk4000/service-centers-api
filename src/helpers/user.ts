import User                                                      from '../models/user/user.model'
import UserToken                                                 from '../models/user/user-token.model'
import { compare }                                               from 'bcrypt'
import ServiceCenterApplication
                                                                 from '../models/service-center/service-center-application.model'
import ServiceCenter                                             from '../models/service-center/service-center.model'
import ServiceCenterDeviceService
                                                                 from '../models/service-center/service-center-device-service.model'
import { ValidationError }                                       from 'sequelize'
import ServiceCenterApplicationDeviceService
                                                                 from '../models/service-center/service-center-application-device-service.model'
import { createApplicationFromModel, IServiceCenterApplication } from './service-centers'


export interface IUser {
  id: number
  firstName: string
  lastName: string
  middleName: string
  email: string
  address: string
  phone: string
}

export interface IUserFull extends IUser {
  applications: IServiceCenterApplication[]
}

export async function createUserFromModel(options: { user: User, full: true }): Promise<IUserFull>
export async function createUserFromModel(options: { user: User, full?: false }): Promise<IUser>
export async function createUserFromModel({ user, full }: { user: User, full?: boolean }): Promise<IUser | IUserFull> {
  const iUser: IUser = {
    id: user.id,
    firstName: user.firstName,
    lastName: user.lastName,
    middleName: user.middleName,
    email: user.email,
    address: user.address,
    phone: user.phone,
  }

  if (full) {
    const applications = user.applications || await user.$get('applications')
    return {
      ...iUser,
      applications: await Promise.all(applications.map(application => createApplicationFromModel({ application }))),
    }
  }

  return iUser
}

/**
 * @description returns User in case of success, error message in case of fail
 */
export async function authUserByToken(token: string): Promise<User | string> {
  const userToken = await UserToken.findOne({ where: { token }, include: [User] })
  if (!userToken) return 'Токен не найден'

  return userToken.user
}

/**
 * @description returns User in case of success, error message in case of fail
 */
export async function authUserByEmailAndPassword(email: string, password: string): Promise<User | string> {
  const user = await User.findOne({ where: { email } })
  if (!user) return 'Пользователь с таким email не найден'

  if (!await compare(password, user.password)) return 'Неверный пароль'

  return user
}

export async function createUserApplication({ user, application }: {
  user: User
  application: IServiceCenterApplication
}): Promise<ServiceCenterApplication> {
  const serviceCenter = await ServiceCenter.findByPk(application.serviceCenterId, { include: [ServiceCenterDeviceService] })
  if (!serviceCenter) throw new ValidationError('Service center not found')

  for (const deviceService of application.devicesServices) {
    if (!serviceCenter.devicesServices.some(x => x.deviceServiceId === deviceService.id))
      throw new ValidationError('Some of devices services not found in service center')
  }

  const serviceCenterApplication = await ServiceCenterApplication.create({
    status: application.status,
    totalCost: application.totalCost,
    userId: user.id,
    serviceCenterId: application.serviceCenterId,
  })

  await Promise.all(application.devicesServices.map(deviceService => ServiceCenterApplicationDeviceService.create({
    serviceCenterApplicationId: serviceCenterApplication.id,
    serviceCenterDeviceServiceId: serviceCenter.devicesServices.find(x => x.deviceServiceId === deviceService.id)?.id,
  })))

  return serviceCenterApplication
}
