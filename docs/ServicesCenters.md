# /services-centers

* `GET /`
    * **Возвращает**: `{ serviceCenters: IServiceCenter[] }`
    
* `POST /` - создание
    * **Требует**: админ
    * **Принимает**: `{serviceCenter: IServiceCenter}`
    
* `PUT /:id` - редактирование
    * **Требует**: админ
    * **Принимает**: `{serviceCenter: IServiceCenter}`
    
* `DELETE /:id` - удаление
    * **Требует**: админ
   
