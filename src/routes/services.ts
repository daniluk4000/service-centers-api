import { Router }                                                     from 'express'
import { userAuthAdmin }                                              from '../middlewares/userAuth'
import jsonResponseUser                                               from '../middlewares/jsonResponseUser'
import { createService, deleteService, editService, getServicesList } from '../controllers/services'

const router = Router()

router.get('/', jsonResponseUser(getServicesList))
router.post('/', userAuthAdmin, jsonResponseUser(createService))
router.put('/:id', userAuthAdmin, jsonResponseUser(editService))
router.delete('/:id', userAuthAdmin, jsonResponseUser(deleteService))

export const servicesRouter = router
