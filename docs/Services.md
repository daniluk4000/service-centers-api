# /services

* `GET /`
    * **Возвращает**: `{ services: IService[] }`
    
* `POST /` - создание
    * **Требует**: админ
    * **Принимает**: `{service: IService}`
    
* `PUT /:id` - редактирование
    * **Требует**: админ
    * **Принимает**: `{service: IService}`
    
* `DELETE /:id` - удаление
    * **Требует**: админ
   
