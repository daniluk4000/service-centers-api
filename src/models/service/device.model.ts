import { AllowNull, AutoIncrement, Column, DataType, HasMany, Model, PrimaryKey, Table } from 'sequelize-typescript'
import DeviceService                                                                     from './device-service.model'

@Table({
  modelName: 'device',
})
export default class Device extends Model<Device> {
  @AutoIncrement
  @PrimaryKey
  @Column(DataType.INTEGER)
  id: number

  @AllowNull(false)
  @Column(DataType.STRING)
  name: string

  @AllowNull(false)
  @Column(DataType.STRING)
  type: string

  @HasMany(() => DeviceService, {onDelete: 'cascade'})
  services: DeviceService[]
}
