import { NextFunction, Response } from 'express'
import { authUserByToken }        from '../helpers/user'

export async function userAuth(req: any, res: Response, next: NextFunction) {
  const token = req.headers.authorization?.replace('Bearer ', '') || ''
  const result = await authUserByToken(token)
  if (typeof result === 'string') return res.status(401).json({
    status: 0,
    error: result,
  })

  req.user = result
  next()
}

export async function userAuthAdmin(req: any, res: Response, next: NextFunction) {
  const token = req.headers.authorization?.replace('Bearer ', '') || ''
  const result = await authUserByToken(token)
  if (typeof result === 'string') return res.status(401).json({
    status: 0,
    error: result,
  })

  if (result.role !== 'admin') return res.status(403).json({
    status: 0,
    error: 'Недостаточно прав доступа',
  })

  req.user = result
  next()
}
