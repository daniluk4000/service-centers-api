# /users

## Текущий пользователь
* `GET /` - получение текущего пользователя
    * **Требует**: авторизованный пользователь
    * **Возвращает**: `{ user: IUserFull }`
    
* `POST /auth` - авторизация пользователя 
    * **Принимает**: `{email, password}`
    * **Возвращает**: `{ token: string, user: IUserFull }`
    
* `POST /register` - регистрация пользователя
    * **Принимает**: `{user: {email, password, firstName, lastName, middleName, address, phone} }`
    * **Возвращает**: `{ token: string, user: IUserFull }`
    
* `PUT /` - редактирование текущего пользователя
    * **Требует**: авторизованный пользователь
    * **Принимает**: `{user: {email, firstName, lastName, middleName, address, phone}}`
    
## Управление
* `GET /list` - список пользователей
    * **Требует**: админ
    * **Возвращает**: `{ users: IUserFull[] }`

* `PUT /list/:id` - редактирование пользователя
    * **Требует**: админ
    * **Принимает**: `{user: {email, firstName, lastName, middleName, address, phone}}`
    
* `DELETE /list/:id` - удаление пользователя, кроме админа
    * **Требует**: админ
