import {
  AllowNull,
  AutoIncrement,
  Column,
  DataType,
  Default,
  HasMany,
  Model,
  PrimaryKey,
  Table,
}                    from 'sequelize-typescript'
import DeviceService from './device-service.model'

@Table({
  modelName: 'service',
})
export default class Service extends Model<Service> {
  @AutoIncrement
  @PrimaryKey
  @Column(DataType.INTEGER)
  id: number

  @AllowNull(false)
  @Column(DataType.STRING)
  name: string

  @Default('')
  @Column(DataType.STRING)
  description: string

  @HasMany(() => DeviceService, {onDelete: 'cascade'})
  devices: DeviceService[]
}
