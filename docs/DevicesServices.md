# /devices-services

* `GET /`
    * **Возвращает**: `{ devicesServices: IDeviceService[] }`
    
* `POST /` - создание
    * **Требует**: админ
    * **Принимает**: `{deviceService: IDeviceService}`
    
* `PUT /:id` - редактирование
    * **Требует**: админ
    * **Принимает**: `{deviceService: IDeviceService}`
    
* `DELETE /:id` - удаление
    * **Требует**: админ
   
