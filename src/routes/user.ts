import { Router }                                                           from 'express'
import jsonResponseUser                                                     from '../middlewares/jsonResponseUser'
import { userDelete, userEdit, userGet, userLogin, userRegister, usersGet } from '../controllers/user'
import { userAuth, userAuthAdmin }                                          from '../middlewares/userAuth'

const router = Router()

router.get('/list', userAuthAdmin, jsonResponseUser(usersGet))
router.put('/list/:id', userAuthAdmin, jsonResponseUser(userEdit))
router.delete('/list/:id', userAuthAdmin, jsonResponseUser(userDelete))

router.get('/', userAuth, jsonResponseUser(userGet))
router.post('/auth', jsonResponseUser(userLogin))
router.post('/register', jsonResponseUser(userRegister))
router.put('/', userAuth, jsonResponseUser(userEdit))

export const usersRouter = router
