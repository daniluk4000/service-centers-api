import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  HasMany,
  Index,
  Model,
  PrimaryKey,
  Table,
}                                 from 'sequelize-typescript'
import Device                     from './device.model'
import Service                    from './service.model'
import ServiceCenterDeviceService from '../service-center/service-center-device-service.model'

@Table({
  tableName: 'devices-services',
})
export default class DeviceService extends Model<DeviceService> {
  @AutoIncrement
  @PrimaryKey
  @Column(DataType.INTEGER)
  id: number

  @AllowNull(false)
  @Index
  @ForeignKey(() => Device)
  @Column(DataType.INTEGER)
  deviceId: number

  @BelongsTo(() => Device, { onDelete: 'cascade' })
  device: Device

  @AllowNull(false)
  @Index
  @ForeignKey(() => Service)
  @Column(DataType.INTEGER)
  serviceId: number

  @BelongsTo(() => Service, { onDelete: 'cascade' })
  service: Service

  @AllowNull(false)
  @Column(DataType.INTEGER)
  cost: number

  @HasMany(() => ServiceCenterDeviceService, { onDelete: 'cascade' })
  serviceCenters: ServiceCenterDeviceService[]
}
