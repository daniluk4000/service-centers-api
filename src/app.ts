import express         from 'express'
import * as bodyParser from 'body-parser'
import cors            from 'cors'
import sequelize       from './connection'
import User            from './models/user/user.model'
import { hash }        from 'bcrypt'
import { indexRouter } from './routes'

class App {
  public app: express.Application

  constructor() {
    this.app = express()
    this.config()
  }

  private async config() {
    this.app.use(bodyParser.json({ limit: '1mb' }))
    this.app.use(cors())
    this.app.use('/', indexRouter)

    await sequelize.authenticate().catch((e) => {
      console.error(e)
      process.exit(0)
    })

    console.info('DB connection has been established')

    await sequelize.sync({ force: false, alter: false }).then(async () => {
      console.log('Sync has been completed')

      if (!await User.findOne({ where: { email: 'admin' } })) {
        await User.create({
          email: 'admin',
          password: await hash('12345678', 8),
          firstName: 'admin',
          lastName: 'admin',
          role: 'admin'
        })
      }
    })

  }

}

export default new App().app
