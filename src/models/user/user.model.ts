import {
  AllowNull,
  AutoIncrement,
  Column,
  DataType,
  Default, HasMany,
  Index,
  Is,
  Model,
  PrimaryKey,
  Table,
}                               from 'sequelize-typescript'
import validator                from 'validator'
import isEmail = validator.isEmail
import UserToken                from './user-token.model'
import ServiceCenterApplication from '../service-center/service-center-application.model'

@Table({
  modelName: 'user',
})
export default class User extends Model<User> {
  @AutoIncrement
  @PrimaryKey
  @Column(DataType.INTEGER)
  id: number

  @AllowNull(false)
  @Column(DataType.STRING)
  firstName: string

  @AllowNull(false)
  @Column(DataType.STRING)
  lastName: string

  @Default('')
  @Column(DataType.STRING)
  middleName: string

  @Is('email', (email: string) => {
    return email === 'admin' || isEmail(email)
  })
  @Index({ unique: true })
  @AllowNull(false)
  @Column(DataType.STRING)
  email: string

  @AllowNull(false)
  @Default('user')
  @Column(DataType.STRING)
  role: string

  @AllowNull(false)
  @Column(DataType.STRING)
  password: string

  @Column(DataType.STRING)
  address: string

  @Column(DataType.STRING)
  phone: string

  @HasMany(() => UserToken, {onDelete: 'cascade'})
  tokens: UserToken[]

  @HasMany(() => ServiceCenterApplication, {onDelete: 'cascade'})
  applications: ServiceCenterApplication[]
}
