import { defaultRequest, jsonResponse }     from '../types'
import { createServiceFromModel, IService } from '../helpers/devices-services'
import Service                              from '../models/service/service.model'

export async function getServicesList(req: defaultRequest): jsonResponse<{ services: IService[] }> {
  return {
    status: 1,
    services: await Promise.all((await Service.findAll()).map(service => createServiceFromModel({ service }))),
  }
}

export async function createService({ body }: defaultRequest): jsonResponse {
  const service: IService = body.service
  await Service.create({
    name: service.name,
    description: service.description,
  })

  return {
    status: 1,
  }
}

export async function editService({ body, params }: defaultRequest): jsonResponse {
  const serviceModel = await Service.findByPk(params.id)
  if (!serviceModel) return {
    status: 0,
    code: 404,
    error: 'Не найдено',
  }

  const service: IService = body.service
  serviceModel.name = service.name
  serviceModel.description = service.description
  await serviceModel.save()

  return {
    status: 1,
  }
}

export async function deleteService({ params }: defaultRequest): jsonResponse {
  const serviceModel = await Service.findByPk(params.id)
  if (!serviceModel) return {
    status: 0,
    code: 404,
    error: 'Не найдено',
  }

  await serviceModel.destroy()

  return {
    status: 1,
  }
}
