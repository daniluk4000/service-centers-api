# /applications

* `GET /`
    * **Требует**: админ
    * **Возвращает**: `{ applications: IServiceCenterApplication[] }`
    
* `POST /` - создание заявки от имени текущего пользователя
    * **Требует**: авторизованный пользователь
    * **Принимает**: `{application: IServiceCenterApplication}`
    
* `PUT /:id` - редактирование
    * **Требует**: админ
    * **Принимает**: `{application: IServiceCenterApplication}`
    
* `DELETE /:id` - удаление
    * **Требует**: авторизованный пользователь или админ, если заявка не принадлежит пользователю
   
