import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  Column,
  DataType,
  ForeignKey, HasMany,
  Index,
  Model,
  PrimaryKey,
  Table,
}                                            from 'sequelize-typescript'
import DeviceService                         from '../service/device-service.model'
import ServiceCenter                         from './service-center.model'
import ServiceCenterApplicationDeviceService from './service-center-application-device-service.model'

@Table({
  tableName: 'service-centers-devices-services',
})
export default class ServiceCenterDeviceService extends Model<ServiceCenterDeviceService> {
  @AutoIncrement
  @PrimaryKey
  @Column(DataType.INTEGER)
  id: number

  @AllowNull(false)
  @Index
  @ForeignKey(() => ServiceCenter)
  @Column(DataType.INTEGER)
  serviceCenterId: number

  @BelongsTo(() => ServiceCenter, { onDelete: 'cascade' })
  serviceCenter: ServiceCenter

  @AllowNull(false)
  @Index
  @ForeignKey(() => DeviceService)
  @Column(DataType.INTEGER)
  deviceServiceId: number

  @BelongsTo(() => DeviceService, { onDelete: 'cascade' })
  deviceService: DeviceService

  @HasMany(() => ServiceCenterApplicationDeviceService, {onDelete: 'cascade'})
  applicationServices: ServiceCenterApplicationDeviceService[]
}
