import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  Index,
  Model,
  PrimaryKey,
  Table,
}           from 'sequelize-typescript'
import User from './user.model'

@Table({
  tableName: 'users-tokens',
})
export default class UserToken extends Model<UserToken> {
  @AutoIncrement
  @PrimaryKey
  @Column(DataType.INTEGER)
  id: number

  @AllowNull(false)
  @Index({ unique: true })
  @Column(DataType.STRING)
  token: string

  @AllowNull(false)
  @ForeignKey(() => User)
  @Column(DataType.INTEGER)
  userId: number

  @BelongsTo(() => User, {onDelete: 'cascade'})
  user: User
}
