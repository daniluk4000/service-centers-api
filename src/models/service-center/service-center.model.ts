import {
  AllowNull,
  AutoIncrement,
  Column,
  DataType,
  Default,
  HasMany,
  Model,
  PrimaryKey,
  Table,
}                                 from 'sequelize-typescript'
import ServiceCenterDeviceService from './service-center-device-service.model'
import ServiceCenterApplication   from './service-center-application.model'

@Table({
  tableName: 'service-centers',
})
export default class ServiceCenter extends Model<ServiceCenter> {
  @AutoIncrement
  @PrimaryKey
  @Column(DataType.INTEGER)
  id: number

  @AllowNull(false)
  @Column(DataType.STRING)
  name: string

  @Default('')
  @Column(DataType.STRING)
  address: string

  @Default('')
  @Column(DataType.STRING)
  phone: string

  @HasMany(() => ServiceCenterDeviceService, { onDelete: 'cascade' })
  devicesServices: ServiceCenterDeviceService[]

  @HasMany(() => ServiceCenterApplication, { onDelete: 'cascade' })
  applications: ServiceCenterApplication[]
}
