import { defaultRequest, jsonResponse }   from '../types'
import Device                             from '../models/service/device.model'
import { createDeviceFromModel, IDevice } from '../helpers/devices-services'

export async function getDevicesList(req: defaultRequest): jsonResponse<{ devices: IDevice[] }> {
  return {
    status: 1,
    devices: await Promise.all((await Device.findAll()).map(device => createDeviceFromModel({ device }))),
  }
}

export async function createDevice({ body }: defaultRequest): jsonResponse {
  const device: IDevice = body.device
  await Device.create({
    name: device.name,
    type: device.type,
  })

  return {
    status: 1,
  }
}

export async function editDevice({ body, params }: defaultRequest): jsonResponse {
  const deviceModel = await Device.findByPk(params.id)
  if (!deviceModel) return {
    status: 0,
    code: 404,
    error: 'Не найдено',
  }

  const device: IDevice = body.device
  deviceModel.name = device.name
  deviceModel.type = device.type
  await deviceModel.save()

  return {
    status: 1,
  }
}

export async function deleteDevice({ params }: defaultRequest): jsonResponse {
  const deviceModel = await Device.findByPk(params.id)
  if (!deviceModel) return {
    status: 0,
    code: 404,
    error: 'Не найдено',
  }

  await deviceModel.destroy()

  return {
    status: 1,
  }
}
