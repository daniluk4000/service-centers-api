import { Router }                                                                 from 'express'
import { userAuth, userAuthAdmin }                                                from '../middlewares/userAuth'
import jsonResponseUser                                                           from '../middlewares/jsonResponseUser'
import { createApplication, deleteApplication, editApplication, getApplications } from '../controllers/applications'
import { createDevice, deleteDevice, editDevice, getDevicesList }                 from '../controllers/devices'

const router = Router()

router.get('/', jsonResponseUser(getDevicesList))
router.post('/', userAuthAdmin, jsonResponseUser(createDevice))
router.put('/:id', userAuthAdmin, jsonResponseUser(editDevice))
router.delete('/:id', userAuthAdmin, jsonResponseUser(deleteDevice))

export const devicesRouter = router
