import { Response }                                      from 'express'
import { defaultRequest, jsonResponse, userAuthRequest } from '../types'

export default function jsonResponseUser(func: (req?: any, res?: Response) => jsonResponse) {
  return async function (req: userAuthRequest | defaultRequest, res, next) {
    let code: number = 200

    func(req, res).then(async (resp) => {
      if (resp.status !== -1 && !res.headersSent) {
        code = resp.code || 200
        delete resp.code
        res.status(code || 200).json(resp)
      }
    }).catch(async (e) => {
      if (!res.headersSent) {
        req.error = e
        console.error(req.error)
        res.status(500).json({
          status: 0,
          error: 'Unknown server error',
        })
      }
      else console.error(e)
    })
  }
}
