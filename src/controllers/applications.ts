import { defaultRequest, jsonResponse, userAuthRequest }         from '../types'
import { createUserApplication }                                 from '../helpers/user'
import ServiceCenterApplication
                                                                 from '../models/service-center/service-center-application.model'
import { createApplicationFromModel, IServiceCenterApplication } from '../helpers/service-centers'

export async function getApplications(): jsonResponse<{ applications: IServiceCenterApplication[] }> {
  return {
    status: 1,
    applications: await Promise.all((await ServiceCenterApplication.findAll()).map(application => createApplicationFromModel({ application }))),
  }
}

export async function createApplication({ user, body }: userAuthRequest): jsonResponse {
  await createUserApplication({ user, application: body.application })

  return {
    status: 1,
  }
}

export async function editApplication({ body, params: { id } }: defaultRequest): jsonResponse {
  const applicationModel = await ServiceCenterApplication.findByPk(id)
  if (!applicationModel) return {
    status: 0,
    code: 404,
    error: 'Не найдено',
  }

  const application: IServiceCenterApplication = body.application

  applicationModel.totalCost = application.totalCost
  applicationModel.status = application.status
  await applicationModel.save()

  return {
    status: 1,
  }
}

export async function deleteApplication({ params: { id }, user }: userAuthRequest): jsonResponse {
  const applicationModel = await ServiceCenterApplication.findByPk(id)
  if (!applicationModel) return {
    status: 0,
    code: 404,
    error: 'Не найдено',
  }

  if (user.role !== 'admin' && applicationModel.userId !== user.id) return {
    status: 0,
    code: 403,
    error: 'Вы не можете удалить чужую запись',
  }

  await applicationModel.destroy()

  return {
    status: 1,
  }
}
