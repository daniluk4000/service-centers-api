import {
  AutoIncrement,
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  Index,
  Model,
  PrimaryKey,
  Table,
}                                 from 'sequelize-typescript'
import ServiceCenterApplication   from './service-center-application.model'
import ServiceCenterDeviceService from './service-center-device-service.model'

@Table({
  tableName: 'service-centers-applications-devices-services',
})
export default class ServiceCenterApplicationDeviceService extends Model<ServiceCenterApplicationDeviceService> {
  @AutoIncrement
  @PrimaryKey
  @Column(DataType.INTEGER)
  id: number

  @Index('sc_app_id')
  @ForeignKey(() => ServiceCenterApplication)
  @Column(DataType.INTEGER)
  serviceCenterApplicationId: number

  @BelongsTo(() => ServiceCenterApplication, { onDelete: 'cascade' })
  serviceCenterApplication: ServiceCenterApplication

  @Index('sc_ds_id')
  @ForeignKey(() => ServiceCenterDeviceService)
  @Column(DataType.INTEGER)
  serviceCenterDeviceServiceId: number

  @BelongsTo(() => ServiceCenterDeviceService, { onDelete: 'cascade' })
  serviceCenterDeviceService: ServiceCenterDeviceService
}
