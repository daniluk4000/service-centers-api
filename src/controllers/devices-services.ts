import { defaultRequest, jsonResponse }                 from '../types'
import { createDeviceServiceFromModel, IDeviceService } from '../helpers/devices-services'
import Service                                          from '../models/service/service.model'
import DeviceService                                    from '../models/service/device-service.model'
import Device                                           from '../models/service/device.model'

export async function getDevicesServicesList(req: defaultRequest): jsonResponse<{ devicesServices: IDeviceService[] }> {
  return {
    status: 1,
    devicesServices: await Promise.all((await DeviceService.findAll({
      include: [
        Device,
        Service,
      ],
    })).map(deviceService => createDeviceServiceFromModel({ deviceService }))),
  }
}

export async function createDeviceService({ body }: defaultRequest): jsonResponse {
  const deviceService: IDeviceService = body.deviceService
  await DeviceService.create({
    deviceId: deviceService.deviceId,
    serviceId: deviceService.serviceId,
    cost: deviceService.cost,
  })

  return {
    status: 1,
  }
}

export async function editDeviceService({ body, params }: defaultRequest): jsonResponse {
  const deviceServiceModel = await DeviceService.findByPk(params.id)
  if (!deviceServiceModel) return {
    status: 0,
    code: 404,
    error: 'Не найдено',
  }

  const deviceService: IDeviceService = body.deviceService
  deviceServiceModel.serviceId = deviceService.serviceId
  deviceServiceModel.deviceId = deviceService.deviceId
  deviceServiceModel.cost = deviceService.cost
  await deviceServiceModel.save()

  return {
    status: 1,
  }
}

export async function deleteDeviceService({ params }: defaultRequest): jsonResponse {
  const deviceServiceModel = await DeviceService.findByPk(params.id)
  if (!deviceServiceModel) return {
    status: 0,
    code: 404,
    error: 'Не найдено',
  }

  await deviceServiceModel.destroy()

  return {
    status: 1,
  }
}
