import Device              from '../models/service/device.model'
import Service             from '../models/service/service.model'
import DeviceService       from '../models/service/device-service.model'
import { ValidationError } from 'sequelize'

export interface IDevice {
  id: number
  name: string
  type: string
}

export interface IService {
  id: number
  name: string
  description: string
}

export interface IDeviceService {
  id: number
  deviceId: number
  device: IDevice
  serviceId: number
  service: IService
  cost: number
}

export async function createDeviceFromModel({ device }: { device: Device }): Promise<IDevice> {
  return {
    id: device.id,
    name: device.name,
    type: device.type,
  }
}

export async function createServiceFromModel({ service }: { service: Service }): Promise<IService> {
  return {
    id: service.id,
    name: service.name,
    description: service.description,
  }
}

export async function createDeviceServiceFromModel({ deviceService }: { deviceService: DeviceService }): Promise<IDeviceService> {
  const device = deviceService.device || await deviceService.$get('device')
  if (!device) throw new ValidationError('Device not found in createDeviceServiceFromModel')

  const service = deviceService.service || await deviceService.$get('service')
  if (!service) throw new ValidationError('Service not found in createDeviceServiceFromModel')

  return {
    id: deviceService.id,
    deviceId: deviceService.deviceId,
    device: await createDeviceFromModel({ device }),
    serviceId: deviceService.serviceId,
    service: await createServiceFromModel({ service }),
    cost: deviceService.cost,
  }
}
