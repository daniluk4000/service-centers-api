import { Router }        from 'express'
import jsonResponseUser  from '../middlewares/jsonResponseUser'
import { userAuthAdmin } from '../middlewares/userAuth'
import {
  createServiceCenter,
  deleteServiceCenter,
  editServiceCenter,
  getServiceCentersList,
}                        from '../controllers/service-centers'

const router = Router()

router.get('/', jsonResponseUser(getServiceCentersList))
router.post('/', userAuthAdmin, jsonResponseUser(createServiceCenter))
router.put('/:id', userAuthAdmin, jsonResponseUser(editServiceCenter))
router.delete('/:id', userAuthAdmin, jsonResponseUser(deleteServiceCenter))

export const serviceCenterRouter = router
